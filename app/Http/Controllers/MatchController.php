<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use App\Repositories\Match\MatchRepositoryInterface;

class MatchController extends Controller {

    private $firstColumn = array();
    private $secondColumn = array();
    private $thirdColumn = array();
    private $mainDiagonal = array();
    private $secondDiagonal = array(); 

    /** @var MatchRepositoryInterface*/
    private $repository;

    public function __construct(MatchRepositoryInterface $repository) {
        $this->repository = $repository;
    }

    public function index() {
        return view('index');
    }

    /**
     * Returns a list of matches
     *
     * TODO it's mocked, make this work :)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function matches() {
        return response()->json($this->repository->all());
    }

    /**
     * Returns the state of a single match
     *
     * TODO it's mocked, make this work :)
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function match($id) {
        return response()->json($this->repository->find($id));
    }

    /**
     * Makes a move in a match
     *
     * TODO it's mocked, make this work :)
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function move($id) {
        $position = Input::get('position');

        $match = $this->repository->find($id);
        $match->board[$position] = $match->next == 1 ? 1 : 2;
        $match->next = $match->board[$position] == 1 ? 2 : 1;
        $match->winner = $this->checkPlayerWin($match);

        return response()->json($this->repository->updateMove($match));
    }

    /**
     * Creates a new match and returns the new list of matches
     *
     * TODO it's mocked, make this work :)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create() {
        $this->repository->create([]);

        return $this->matches();
    }

    /**
     * Deletes the match and returns the new list of matches
     *
     * TODO it's mocked, make this work :)
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id) {
        $this->repository->delete($id);

        return $this->matches();
    }

    private function checkPlayerWin($match) {
        $boardChunk = array_chunk($match->board, 3, true);

        foreach($boardChunk as $value) {
            if (array_sum($value) == 3 && count(array_unique($value)) == 1 ) {
                return 1;
            }
            elseif (array_sum($value) == 6 && count(array_unique($value)) == 1) {
                return 2;
            }

            $this->prepareBoardValues($value);
        }
        
        if ((array_sum($this->firstColumn) == 3 && count(array_unique($this->firstColumn)) == 1 )
            || (array_sum($this->secondColumn) == 3 && count(array_unique($this->secondColumn)) == 1)
            || (array_sum($this->thirdColumn) == 3 && count(array_unique($this->thirdColumn)) == 1)
            || (array_sum($this->mainDiagonal) == 3 && count(array_unique($this->mainDiagonal)) == 1)
            || (array_sum($this->secondDiagonal) == 3 && count(array_unique($this->secondDiagonal)) == 1)){

            return 1;
        }
        elseif ((array_sum($this->firstColumn) == 6 && count(array_unique($this->firstColumn)) == 1 )
            || (array_sum($this->secondColumn) == 6 && count(array_unique($this->secondColumn)) == 1)
            || (array_sum($this->thirdColumn) == 6 && count(array_unique($this->thirdColumn)) == 1)
            || (array_sum($this->mainDiagonal) == 6 && count(array_unique($this->mainDiagonal)) == 1)
            || (array_sum($this->secondDiagonal) == 6 && count(array_unique($this->secondDiagonal)) == 1)){

            return 2;
        }
        return 0;
    }

    private function prepareBoardValues($value){
        $keys = array_keys($value);

        for($i=0; $i < count($value); $i++) {
            if($keys[0] == 0){
                if($keys[$i] == 0){
                    array_push($this->firstColumn, $value[0]);
                    array_push($this->secondColumn, $value[1]);
                    array_push($this->thirdColumn, $value[2]);
                    array_push($this->mainDiagonal, $value[0]);
                }
                elseif($keys[$i] == 2){
                    array_push($this->secondDiagonal, $value[2]);
                }
            }
            if($keys[0] == 3){
                if($keys[$i] == 4){
                    array_push($this->firstColumn, $value[3]);
                    array_push($this->secondColumn, $value[4]);
                    array_push($this->thirdColumn, $value[5]);
                    array_push($this->mainDiagonal, $value[4]);
                    array_push($this->secondDiagonal, $value[4]);
                }
            }
            if($keys[0] == 6){
                if($keys[$i] == 8){
                    array_push($this->firstColumn, $value[6]);
                    array_push($this->secondColumn, $value[7]);
                    array_push($this->thirdColumn, $value[8]);
                    array_push($this->mainDiagonal, $value[8]);
                }
                elseif($keys[$i] == 6){
                    array_push($this->secondDiagonal, $value[6]);
                }
            }
        }
    }
}