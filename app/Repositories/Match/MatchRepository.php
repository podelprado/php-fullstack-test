<?php

namespace App\Repositories\Match;

use App\Repositories\Match\MatchRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;

class MatchRepository implements MatchRepositoryInterface
{
    public function all() {
        return DB::table('matches')
            ->whereNull('deleted_at')
            ->get();
    }

    public function create(array $data = []) {
        $board = array(0,0,0,0,0,0,0,0,0);
        $nameId = $this->getLastId() + 1;

        $id = DB::table('matches')->insertGetId(
            [
                'name'   => 'Match' . $nameId,
                'next'   => 1,
                'winner' => 0,
                'board'  => json_encode($board)
            ]
        );

        if(empty($id)) {
            throw new ModelNotFoundException("Error creating match");
        }
    }

    public function update(array $data, $id) { }

    public function delete($id) {
        $result = DB::table('matches')
            ->where('id', '=', $id)
            ->update(['deleted_at' => Carbon::now()]);
        
        if(!$result) {
            throw new ModelNotFoundException("Error deleting match");
        }
    }

    public function find($id) {
        $match = DB::table('matches')
            ->select('id','name','next','winner','board')
            ->where('id', '=', $id)
            ->whereNull('deleted_at')
            ->get()[0];

        if ($match == null) {
            throw new ModelNotFoundException("Match not found");
        }

        $match->board = json_decode($match->board);

        return $match;
    }

    public function updateMove($match) {
        $matchArray = array(
            'name' => $match->name,
            'next' => $match->next,
            'winner' => $match->winner,
            'board' => json_encode($match->board)
        );

        $result = DB::table('matches')
            ->where('id', '=', $match->id)
            ->update($matchArray);
        
        if($result) {
            return $match;
        }
        else {
            throw new ModelNotFoundException("Error uploading move");
        }
    }
    
    private function getLastId() {
        return DB::table('matches')
            ->max('id');
    }
}