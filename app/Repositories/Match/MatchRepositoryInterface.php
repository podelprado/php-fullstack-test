<?php
namespace App\Repositories\Match;

use App\Repositories\RepositoryInterface;

interface MatchRepositoryInterface extends RepositoryInterface
{
    public function updateMove($match);
}